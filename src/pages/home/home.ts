import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { SignupPage } from '../signup/signup';
import { LoginPage } from '../login/login';
import { TabsPage } from '../tabs/tabs';

import { AngularFire, FirebaseListObservable } from 'angularfire2';
import { AuthService } from '../../providers/auth-service';


/*
  Generated class for the Home page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  items: FirebaseListObservable<any[]>;

  constructor(public navCtrl: NavController,af: AngularFire,private _auth: AuthService) {
    this.items = af.database.list('/');
    console.log("========items============");
    console.log(this.items);
    console.log("========items============")
  }
  ionViewDidLoad() {
    console.log('ionViewDidLoad HomePage');
  }
  
  onLogin() {
      this.navCtrl.push(LoginPage);
  }

  onSignup() {
    this.navCtrl.push(SignupPage);
  }

   private onSignInSuccess(): void {
    console.log("Facebook display name ",this._auth.displayName());
  }

}
