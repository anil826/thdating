import { Component } from '@angular/core';

import { PopoverController } from 'ionic-angular';

import { PopoverPage } from '../slide3-popover/slide3-popover';

@Component({
  selector: 'page-slide3',
  templateUrl: 'slide3.html'


})
export class Slide3Page {
  conferenceDate = '2047-05-17';

  constructor(public popoverCtrl: PopoverController) { }

  presentPopover(event: Event) {
    let popover = this.popoverCtrl.create(PopoverPage);
    popover.present({ ev: event });
  }

}


