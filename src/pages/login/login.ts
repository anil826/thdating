import { Component } from '@angular/core';
import { NgForm } from '@angular/forms';

import { NavController } from 'ionic-angular';

import { SignupPage } from '../signup/signup';
import { TabsPage } from '../tabs/tabs';

import { AngularFire, FirebaseListObservable } from 'angularfire2';
import { AuthService } from '../../providers/auth-service';


@Component({
  selector: 'page-user',
  templateUrl: 'login.html'
})
export class LoginPage {
  login: {username?: string, password?: string} = {};
  submitted = false;

  constructor(public navCtrl: NavController,af: AngularFire,private _auth: AuthService) { }

  onLogin(form: NgForm) {
  
  }

  onSignup() {
      this._auth.signInWithFacebook()
      .then(() => this.onSignInSuccess());  
    }

    private onSignInSuccess(): void {
    console.log("Facebook display name ",this._auth.displayName());
    this.navCtrl.push(TabsPage);
  }
}
